# Automated AMI Creation with Packer and GitLab CI/CD

## Overview

This repository contains the necessary configuration files to automate the creation of an Amazon Machine Image (AMI) using HashiCorp Packer within a GitLab CI/CD pipeline. The pipeline automates the process of initializing Packer, building the AMI based on an Amazon Linux 2 base image, and notifying stakeholders upon successful completion. The AMI is configured to run a WordPress CMS along with an Apache web server, PHP, and a MySQL server.

## Pipeline Stages

The pipeline is divided into three main stages:

1. **Prepare**: Initializes Packer with the required plugins.
2. **Build**: Executes the Packer build to create the AMI based on the provided configuration.
3. **Notify**: Sends a success notification email to a predefined list of recipients.

## Prerequisites

To use this pipeline, you need:

- A GitLab account with a project for this repository.
- A runner configured with Docker support.
- AWS credentials with permissions to create AMIs.
- The `hashicorp/packer:1.10` Docker image.

## Configuration

Before running the pipeline, make sure to:

1. Set your AWS credentials (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) as CI/CD variables in your GitLab project.
2. Modify the `.gitlab-ci.yml` file to point to the correct location of your email sending script.

## Running the Pipeline

The pipeline is triggered automatically upon pushing changes to the repository. You can also manually trigger the pipeline through the GitLab UI.

## Customization

You can customize the Packer template (`wordpress-ami.pkr.hcl`) to fit your specific requirements for the AMI. Additionally, you can modify the email notification script to change the message format or use a different email delivery service.

## Support

For support and further assistance, please reach out to the repository maintainers.

---

This project aims to simplify the AMI creation process, ensuring consistent and repeatable builds through automation.
