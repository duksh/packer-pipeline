packer {
  required_plugins {
    amazon = {
      source  = "github.com/hashicorp/amazon"
      version = "~> 1"
    }
  }
}

source "amazon-ebs" "wp-pkrush-pipe" {
  # remember to export you AWS profile containing your aws_access_key aws_secret_key
  region                      = "eu-west-1"
  source_ami                  = "ami-09961f5df132ebff4" //AWS Linux 2
  instance_type               = "t2.micro"
  ssh_username                = "ec2-user"
  ami_name                    = "duksh-packer-sh-wordpress-{{timestamp}}"
  vpc_id                      = "vpc-04c8d0e7c89ce138a"
  subnet_id                   = "subnet-031f71ecd7dcbaf77"
  security_group_id           = "sg-0d9280a50deb4039b"
  associate_public_ip_address = true
  ssh_interface               = "public_ip"
  ssh_timeout                 = "10m"             # Increase timeout if needed
  temporary_key_pair_name     = "packer_temp_key_{{timestamp}}" # Custom name for the key pair

  run_tags = {
    Name         = "Packer by Gitlab Pipiline"
    BuiltBy      = "Packer"
    Env          = "SBX"
    Application  = "Packer"
    Owner        = "duksh.k@teamwork.net"
    Description  = "Testing Packer with CICD pipeline"
    BusinessUnit = "TWMU"
    Schedule     = "24h_24h_7j_7j"
  }
}

build {
  sources = ["source.amazon-ebs.wp-pkrush-pipe"]

#TODO - convert to Ansible also
  provisioner "shell" {
    inline = [
      "sudo yum update -y",
      "sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2",
      "sudo yum install -y httpd mariadb-server",
      "sudo systemctl start httpd",
      "sudo systemctl enable httpd",
      "sudo systemctl start mariadb",
      "sudo systemctl enable mariadb",
      "sudo wget https://wordpress.org/latest.tar.gz",
      "sudo tar -xzf latest.tar.gz",
      "sudo cp -r wordpress/* /var/www/html/",
      "sudo chown -R apache:apache /var/www/html/*",
      "sudo systemctl restart httpd"
    ]
  }
}
